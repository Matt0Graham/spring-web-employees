package com.citi.training.employees.exceptions;

@SuppressWarnings("serial")
public class EmployeeNotFoundException extends RuntimeException {

    public EmployeeNotFoundException(String msg) {
        super(msg);
    }
}
